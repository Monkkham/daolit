<div wire:poll>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h6><i class="fas fa-chart-line"></i>
                        ລາຍງານ
                        <i class="fa fa-angle-double-right"></i>
                        ລາຍງານລວມ
                    </h6>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('backend.dashboard') }}">
                                {{ __('lang.home') }}</a>
                        </li>
                        <li class="breadcrumb-item active"> ລາຍງານລວມ</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="date" wire:model="start_date" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="date" wire:model="end_date" class="form-control">
                                    </div>
                                </div>

                                {{-- <div class="col-md-1">
                                    <div class="form-group">
                                        <select name="" wire:model="" class="form-control">
                                            <option value="">{{ __('lang.show') }}</option>
                                            <option value="10" selected>10</option>
                                            <option value="30">30</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="all">{{ __('lang.all') }}</option>
                                        </select>
                                    </div>
                                </div> --}}
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <button class="btn btn-success" onclick="ExportExcel('xlsx')"><i
                                                class="fas fa-print"></i>
                                            Excel</button>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <button class="btn btn-info" id="print"><i class="fas fa-print"></i>
                                            ພິມອອກ</button>
                                    </div>
                                </div>
                            </div><!-- end div-row -->
                        </div>
                        <hr>
                        <div class="row p-3">
                            <div class="col-md-12">
                                <div class="right_content">
                                    {{-- //////////////////// Bill Header /////////////////////////// --}}
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <h6>
                                                {{-- @if (!empty(auth()->user()->branch))
                                                    {!! auth()->user()->branch->header_bill !!}
                                                @endif --}}
                                            </h6>
                                        </div>
                                    </div>
                                    {{-- //////////////////// Bill Header /////////////////////////// --}}
                                    {{-- <div class="row">
                                        <div class="col-md-3 text-center">

                                        </div>
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6 text-right">
                                            <h6>ວັນທີ: {{ date('d/m/Y') }}</h6>
                                            <h6>ເວລາ: {{ date('H:i:s') }}</h6>
                                        </div>
                                    </div> --}}
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <h3>
                                                <b>
                                                    <u>ສະຫລຸບລວມ</u>
                                                </b>
                                            </h3>
                                            <h4><b>
                                                    @if (!empty($this->start_date))
                                                        ວັນທີ:
                                                        {{ date('d-m-Y', strtotime($this->start_date)) }}
                                                    @endif ຫາ
                                                    @if (!empty($this->end_date))
                                                        ວັນທີ:
                                                        {{ date('d-m-Y', strtotime($this->end_date)) }}
                                                    @endif
                                                </b></h4>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-bordered" id="table-excel">
                                                <thead>
                                                    <tr class="text-center h6">
                                                        <th class="bg-success text-success">
                                                            ລວມລາຍຮັບ</th>
                                                        <th class="bg-danger text-danger">
                                                            ລວມລາຍຈ່າຍ</th>
                                                        <th class="bg-warning text-warning">ຜິດດ່ຽງ
                                                        </th>
                                                    </tr>
                                                    <tr class="text-center h6">
                                                        <td class="text-bold text-success">
                                                            {{ number_format($sum_total_income + $sum_money_income) }}
                                                            ₭</td>
                                                        <td class="text-bold text-danger">
                                                            {{ number_format($sum_total_expend + $sum_money_expend) }}
                                                            ₭</td>
                                                        <td class="text-bold text-warning">
                                                            {{ number_format(($sum_total_income + $sum_money_income) - ($sum_total_expend + $sum_money_expend)) }}

                                                            ₭</td>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                    {{-- @include('livewire.backend.report.all-form-content') --}}
                                </div>
                            </div>
                        </div>
                    </div><!-- end card body-->
                </div><!-- end card -->
            </div>
        </div>
    </section>
</div>
@push('scripts')
    <script>
        window.addEventListener('show-transport-detail', event => {
            $('#transport-detail').modal('show');
        })
        window.addEventListener('hide-transport-detail', event => {
            $('#transport-detail').modal('hide');
        })
        window.addEventListener('show-add-oil-detail', event => {
            $('#add-oil-detail').modal('show');
        })
        window.addEventListener('hide-add-oil-detail', event => {
            $('#add-oil-detail').modal('hide');
        })
        window.addEventListener('show-print-bill', event => {
            $('#print-bill').modal('show');
        })
        window.addEventListener('hide-print-bill', event => {
            $('#print-bill').modal('hide');
        })

        $(document).ready(function() {
            $('#table_id').select2();
            $('#table_id').on('change', function(e) {
                var data = $('#table_id').select2("val");
                @this.set('table_id', data);
            });
        });
        $('#print').click(function() {
            printDiv();

            function printDiv() {
                var printContents = $(".right_content").html();
                var originalContents = document.body.innerHTML;
                document.body.innerHTML = printContents;
                window.print();
                document.body.innerHTML = originalContents;
            }
            location.reload();
        });

        function ExportExcel(type, fn, dl) {
            var elt = document.getElementById('table-excel');
            var wb = XLSX.utils.table_to_book(elt, {
                sheet: "Sheet JS"
            });
            return dl ?
                XLSX.write(wb, {
                    bookType: type,
                    bookSST: true,
                    type: 'base64'
                }) :
                XLSX.writeFile(wb, fn || ('report_all.' + (type || 'xlsx')));
        }
    </script>
    @push('scripts')
        <script>
            function printPage() {
                var printContent = document.getElementById('printContent').innerHTML;
                var originalContent = document.body.innerHTML;
                document.body.innerHTML = printContent;
                window.print();
                document.body.innerHTML = originalContent;
                location.reload();
            }
        </script>
    @endpush
@endpush
