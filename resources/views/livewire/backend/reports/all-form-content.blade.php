                                    {{-- ===================== ລາຍຮັບຂົນສົ່າງ=============================== --}}
                                    {{-- @if (count($list_income_transport) > 0) --}}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h3 class="text-success"><b><i class="fas fa-truck-moving"></i>
                                                    {{ __('lang.clear_link_transport') }}</b></h3>
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <thead class="bg-light">
                                                        <tr class="text-center">
                                                            <th class="text-center">{{ __('lang.no') }}</th>
                                                            <th>{{ __('lang.date') }}</th>
                                                            <th>ແມັດກ້ອນຊັງ</th>
                                                            <th>ລວມຖ້ຽວ</th>
                                                            <th>ລວມM3</th>
                                                            <th>ຄ່າແກ່/M3</th>
                                                            <th>{{ __('lang.total_price') }}</th>
                                                            <th> <button class="btn btn-primary btn-sm" type="button"
                                                                    wire:click.live="ShowPrint"><i
                                                                        class="fas fa-print"></i></button></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $i = 1;
                                                        @endphp
                                                        @foreach ($list_income_transport as $item)
                                                            <tr class="text-center">
                                                                <td>{{ $i++ }}</td>
                                                                <td>{{ date('d/m/Y', strtotime($item->created_at)) }}
                                                                </td>
                                                                <td class="text-bold">
                                                                    {{ number_format($item->kilogarms) }}</td>
                                                                <td class="text-bold">
                                                                    {{ number_format($item->ids) }}</td>
                                                                <td class="text-bold">
                                                                    {{ number_format($item->sum_kilogarms) }}</td>
                                                                <td class="text-bold">
                                                                    {{ number_format($item->transport_prices) }} ₭</td>
                                                                <td class="text-bold">
                                                                    {{ number_format($item->total_transport_paids) }} ₭
                                                                </td>
                                                                <td>
                                                                    <button
                                                                        wire:click="ShowDetailIncomeTransport({{ $item->id }})"
                                                                        class="btn btn-info btn-sm fas fa-eye"></button>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        <tr class="h5 bg-light">
                                                            <td colspan="3" class="text-right">
                                                                <b>{{ __('lang.total') }}</b>
                                                            </td>
                                                            <td class="text-center">
                                                                <b>
                                                                    {{ number_format($this->count_transport) }}</b>
                                                            </td>
                                                            <td class="text-center">
                                                                <b>
                                                                    {{ number_format($this->sum_income_transport_kilogram) }}</b>
                                                            </td>
                                                            <td class="text-center">

                                                            </td>
                                                            <td class="text-center">
                                                                <b>
                                                                    {{ number_format($this->sum_income_total_transport_paid) }}</b>
                                                                ₭
                                                            </td>
                                                            <td class="text-center">
                                                                <b>
                                                                </b>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- @endif --}}
                                    {{-- ===================== ລາຍການລາຍຮັບປະຈຳວັນ=============================== --}}
                                    {{-- @if (count($list_income_everyday) > 0) --}}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h3 class="text-success"><b><i class="fas fa-money"></i>
                                                    {{ __('lang.income_everyday') }}</b></h3>
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <thead class="bg-light">
                                                        <tr class="text-center">
                                                            <th class="text-center">{{ __('lang.no') }}</th>
                                                            <th>{{ __('lang.date') }}</th>
                                                            <th>{{ __('lang.name') }}</th>
                                                            <th>{{ __('lang.subtotal') }}</th>
                                                            <th>{{ __('lang.status') }}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $i = 1;
                                                        @endphp
                                                        @foreach ($list_income_everyday as $item)
                                                            <tr class="text-center">
                                                                <td>{{ $i++ }}</td>
                                                                <td>{{ date('d/m/Y', strtotime($item->created_at)) }}
                                                                </td>
                                                                <td>{{ $item->name }}</td>
                                                                <td>
                                                                    {{ number_format($item->total_price) }} ₭
                                                                </td>
                                                                <td>
                                                                    @if ($item->status == '1')
                                                                        <span class="text-success">ແປງລົດ</span><br>
                                                                        @if (!empty($item->car))
                                                                            ({{ $item->car->register_number }})
                                                                        @endif
                                                                    @elseif($item->status == '2')
                                                                        <span class="text-danger">ບໍລິຫານ</span>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        <tr class="h5 bg-light">
                                                            <td colspan="3" class="text-right">
                                                                <b>{{ __('lang.total') }}</b>
                                                            </td>
                                                            <td class="text-center">
                                                                <b>
                                                                    {{ number_format($this->sum_income_everyday) }} ₭
                                                                </b>
                                                            </td>
                                                            <td class="text-center">
                                                                <b>

                                                                </b>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- @endif --}}
                                    {{-- ===================== ລາຍການລາຍຈ່າຍຄ່ານໍ້າມັນ =============================== --}}
                                    {{-- @if (count($list_expend_oil) > 0) --}}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h3 class="text-danger"><b><i class="fas fa-gas-pump"></i>
                                                    {{ __('lang.clear_link_oil') }}</b></h3>
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <thead class="bg-light">
                                                        <tr>
                                                            <th colspan="9" class="text-center text-primary">
                                                                ພາກລາຍຈ່າຍນໍ້າມັນ</th>
                                                            <th colspan="4" class="text-center text-info">ພາກກົງເຕີ
                                                            </th>
                                                        </tr>
                                                        <tr class="text-center">
                                                            <th class="text-center">{{ __('lang.no') }}</th>
                                                            <th>{{ __('lang.date') }}</th>
                                                            <th>{{ __('lang.car') }}</th>
                                                            <th>ນໍ້າມັນຄ້າງຖັງເຊົ້າ</th>
                                                            <th>ລວມນໍ້າມັນເຕີມ</th>
                                                            <th>ນໍ້າມັນຄ້າງຖັງແລງ</th>
                                                            <th>ນໍ້າມັນທີ່ໃຊ້ໄປມື້ນີ້</th>
                                                            <th>ລາຄານໍ້າມັນ/ລິດ</th>
                                                            <th>{{ __('lang.subtotal') }}</th>
                                                            <th>ກົງເຕີລວມມື້ນີ້</th>
                                                            <th>ລວມນໍ້າມັນມື້</th>
                                                            <th>ສະເລ່ຍໃຊ້ນໍ້າມັນ</th>
                                                            <th>{{ __('lang.action') }}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $i = 1;
                                                        @endphp
                                                        @foreach ($list_expend_oil as $item)
                                                            <tr class="text-center">
                                                                <td>{{ $i++ }}</td>
                                                                <td>{{ date('d/m/Y', strtotime($item->created_at)) }}
                                                                </td>
                                                                <td>
                                                                    @if (!empty($item->car))
                                                                        {{ $item->car->register_number }}
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    {{ $item->qty_oil_open }}
                                                                </td>
                                                                <td>
                                                                    {{ $item->qty_oil_new }}
                                                                </td>
                                                                <td>
                                                                    {{ $item->qty_oil_off }}
                                                                </td>
                                                                <td>
                                                                    {{ $item->qty_oil_open + $item->qty_oil_new - $item->qty_oil_off }}
                                                                </td>

                                                                <td>
                                                                    {{ number_format($item->price_oil) }} ₭
                                                                </td>
                                                                <td>
                                                                    {{ number_format(($item->qty_oil_open + $item->qty_oil_new - $item->qty_oil_off) * ($item->price_oil)) }}
                                                                    ₭
                                                                </td>
                                                                <td>
                                                                    {{ $item->all_km }}
                                                                </td>
                                                                <td>
                                                                    {{ $item->qty_oil_open + $item->qty_oil_new - $item->qty_oil_off }}
                                                                </td>
                                                                <td>
                                                                    @if (!empty($item->qty_oil_open && $item->qty_oil_new && $item->all_km))
                                                                        {{ number_format((($item->qty_oil_open + $item->qty_oil_new - $item->qty_oil_off) / $item->all_km) * 2, 9) }}
                                                                    @else
                                                                        0
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    <button
                                                                        wire:click="ShowDetailExpendAddoil({{ $item->id }})"
                                                                        class="btn btn-info btn-sm fas fa-eye"></button>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        <tr class="h5 bg-light">
                                                            <td colspan="2" class="text-right">
                                                                <b>{{ __('lang.total') }}</b>
                                                            </td>
                                                            <td class="text-center">
                                                                <b>
                                                                    {{ $this->sum_expend_qty_oil_off }}</b>
                                                            </td>
                                                            <td class="text-center">
                                                                <b>
                                                                    {{ $this->sum_expend_qty_oil_new }}</b>
                                                            </td>
                                                            <td class="text-center">
                                                                <b>
                                                                    {{ $this->sum_expend_qty_oil_open }}</b>
                                                            </td>
                                                            <td class="text-center">
                                                                <b>
                                                                    {{ $this->sum_expend_qty_oil_off + $this->sum_expend_qty_oil_new - $this->sum_expend_qty_oil_open }}</b>
                                                            </td>
                                                            <td class="text-center">
                                                                <b>

                                                                </b>
                                                            </td>
                                                            <td class="text-center">
                                                                <b>
                                                                </b>
                                                            </td>
                                                            <td class="text-center">
                                                                <b>
                                                                    {{ number_format(($this->sum_expend_qty_oil_off + $this->sum_expend_qty_oil_new - $this->sum_expend_qty_oil_open) * $this->select_expend_qty_oil_price) }}
                                                                    ₭
                                                                </b>

                                                            </td>
                                                            <td class="text-center">
                                                                <b>
                                                                </b>
                                                            </td>
                                                            <td class="text-center">
                                                                <b>
                                                                </b>
                                                            </td>
                                                            <td class="text-center">
                                                                <b>
                                                                </b>
                                                            </td>
                                                            <td class="text-center">
                                                                <b>
                                                                </b>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- @endif --}}
                                    {{-- ===================== ລາຍການລາຍຈ່າຍຄ່າໂຊເຟີ້ =============================== --}}
                                    {{-- @if ($count_expend_transport) --}}
                                    {{-- <div class="row">
                                        <div class="col-md-12">
                                            <h3 class="text-danger"><b><i class="fas fa-users"></i>
                                                    {{ __('lang.clear_link_for_driver') }}</b></h3>
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <thead class="bg-light">
                                                        <tr class="text-center">
                                                            <th>ລວມຈຳນວນທ້ຽວ</th>
                                                            <th>{{ __('lang.price') }}</th>
                                                            <th>{{ __('lang.total_price') }}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr class="text-center">
                                                            <td>{{ $this->count_expend_transport }}</td>
                                                            <td>{{ number_format($this->sum_expend_transport_price) }}
                                                            </td>
                                                            <td>{{ number_format($this->count_expend_transport * $this->sum_expend_transport_price) }}
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div> --}}
                                    {{-- @endif --}}
                                    {{-- ===================== ລາຍການລາຍຈ່າຍປະຈຳວັນ=============================== --}}
                                    {{-- @if (count($list_expend_everyday) > 0) --}}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h3 class="text-danger"><b><i class="fas fa-money"></i>
                                                    {{ __('lang.expend_everyday') }}</b></h3>
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <thead class="bg-light">
                                                        <tr class="text-center">
                                                            <th class="text-center">{{ __('lang.no') }}</th>
                                                            <th>{{ __('lang.date') }}</th>
                                                            <th>{{ __('lang.name') }}</th>
                                                            <th>{{ __('lang.subtotal') }}</th>
                                                            <th>{{ __('lang.status') }}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $i = 1;
                                                        @endphp
                                                        @foreach ($list_expend_everyday as $item)
                                                            <tr class="text-center">
                                                                <td>{{ $i++ }}</td>
                                                                <td>{{ date('d/m/Y', strtotime($item->created_at)) }}
                                                                </td>
                                                                <td>{{ $item->name }}</td>
                                                                <td>
                                                                    {{ number_format($item->total_price) }} ₭
                                                                </td>
                                                                <td>
                                                                    @if ($item->status == '1')
                                                                        <span class="text-success">ແປງລົດ</span><br>
                                                                        @if (!empty($item->car))
                                                                            {{-- ({{ $item->car->register_number }}) --}}
                                                                        @endif
                                                                    @elseif($item->status == '2')
                                                                        <span class="text-danger">ບໍລິຫານ</span>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        <tr class="h5 bg-light">
                                                            <td colspan="3" class="text-right">
                                                                <b>{{ __('lang.total') }}</b>
                                                            </td>
                                                            <td class="text-center">
                                                                <b>
                                                                    {{ number_format($this->sum_expend_everyday) }} ₭
                                                                </b>
                                                            </td>
                                                            <td class="text-center">
                                                                <b>

                                                                </b>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- @endif --}}
                                    {{-- =============== ລາຍລະອຽດຂົນສົ່ງ ================= --}}
                                    <div wire:ignore.self class="modal fabe" id="transport-detail">
                                        <div class="modal-dialog modal-xl">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title text-success">
                                                        <i class="fas fa-truck-moving"></i>
                                                        {{ __('lang.detail') }}{{ __('lang.income') }}{{ __('lang.transports') }}
                                                    </h5>
                                                    <button class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form>
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="table-responsive">
                                                                    <table
                                                                        class="table table-bordered table-striped text-center">
                                                                        <thead>
                                                                            <tr class="bg-light text-bold h5">
                                                                                <th colspan="10">
                                                                                    @if (!empty($this->car_data))
                                                                                        {{ $this->car_data }}
                                                                                    @endif
                                                                                </th>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>{{ __('lang.no') }}</th>
                                                                                <th>{{ __('lang.date') }}</th>
                                                                                <th>ຕົ້ນທາງ</th>
                                                                                <th>ປາຍທາງ</th>
                                                                                {{-- <th>ປາຍທາງ</th> --}}
                                                                                <th>{{ __('lang.customer') }}</th>
                                                                                {{-- <th>{{ __('lang.driver') }}</th> --}}
                                                                                {{-- <th>{{ __('lang.km') }}</th>
                                                <th>{{ __('lang.time') }}</th> --}}
                                                                                <th>{{ __('lang.kg_qty') }}</th>
                                                                                <th>{{ __('lang.price') }}</th>
                                                                                <th>{{ __('lang.total_paid') }}</th>
                                                                                {{-- <th>ລາຄາ/ດິນ</th> --}}
                                                                                <th>{{ __('lang.status') }}</th>
                                                                                <th>{{ __('lang.type') }}</th>
                                                                                {{-- <th>{{ __('lang.creator') }}</th> --}}
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            @php
                                                                                $i = 1;
                                                                            @endphp
                                                                            @foreach ($list_transport as $item)
                                                                                <tr>
                                                                                    <td>{{ $i++ }}</td>
                                                                                    <td>{{ date('d/m/Y', strtotime($item->created_at)) }}
                                                                                        <br>
                                                                                        {{ date('H:i:s', strtotime($item->created_at)) }}
                                                                                    </td>
                                                                                    <td>
                                                                                        @if (!empty($item->start_end_streets->bor))
                                                                                            {{ $item->start_end_streets->bor->name }}
                                                                                        @endif
                                                                                    </td>
                                                                                    <td>
                                                                                        @if (!empty($item->address))
                                                                                            {{ $item->address }}
                                                                                        @else
                                                                                            -
                                                                                        @endif
                                                                                    </td>
                                                                                    <td>
                                                                                        @if (!empty($item->customer))
                                                                                            {{ $item->customer->firstname }}
                                                                                            <br>
                                                                                            {{ $item->customer->phone }}
                                                                                        @endif
                                                                                    </td>
                                                                                    {{-- <td>
                                                    @if (!empty($item->open_kar->user))
                                                    {{ $item->open_kar->user->firstname }} <br>
                                                    {{ $item->open_kar->user->phone }}
                                                    @endif
                                                </td> --}}
                                                                                    {{-- <td>{{ number_format($item->km) }} km</td>
                                                <td>{{ number_format(($item->km * 5) / 3.8, 2) }} ນາທີ</td> --}}
                                                                                    <td>{{ number_format($item->kilogarm) }}
                                                                                    </td>
                                                                                    <td>{{ number_format($item->transport_price) }}
                                                                                    </td>
                                                                                    <td class="text-bold">
                                                                                        {{ number_format($item->total_transport_paid) }}
                                                                                    </td>
                                                                                    {{-- <td class="text-bold">
                                                    {{ number_format($item->total_transport_price) }}
                                                </td> --}}
                                                                                    <td>
                                                                                        @if ($item->total_transport_price <= 0 && $item->total_transport_paid <= 0)
                                                                                            <span class="text-success">
                                                                                                {{ __('lang.no_data') }}</span>
                                                                                        @elseif($item->total_transport_price == $item->total_transport_paid)
                                                                                            <span
                                                                                                class="text-success"><i
                                                                                                    class="fas fa-check-circle"></i>
                                                                                                {{ __('lang.pay_success') }}</span>
                                                                                        @elseif($item->total_transport_price > $item->total_transport_paid)
                                                                                            <span
                                                                                                class="text-danger"><i
                                                                                                    class="fas fa-hand-holding-usd"></i>
                                                                                                {{ __('lang.pay_not') }}</span>
                                                                                        @endif
                                                                                    </td>
                                                                                    <td>
                                                                                        @if ($item->payment_type == 1)
                                                                                            <span
                                                                                                class="text-primary">{{ __('lang.cash') }}</span>
                                                                                        @elseif($item->payment_type == 2)
                                                                                            <span
                                                                                                class="text-danger">{{ __('lang.transfer') }}</span>
                                                                                        @else
                                                                                            <span>-</span>
                                                                                        @endif
                                                                                    </td>
                                                                                    {{-- <td>
                                                    @if (!empty($item->user))
                                                    {{ $item->user->firstname }} <br>
                                                    @endif
                                                </td> --}}
                                                                                </tr>
                                                                            @endforeach
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- =============== ລາຍລະອຽດໃສ່ນໍ້າມັນ ================= --}}
                                    <div wire:ignore.self class="modal fabe" id="add-oil-detail">
                                        <div class="modal-dialog modal-xl">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title text-danger">
                                                        <i class="fas fa-gas-pump"></i>
                                                        {{ __('lang.detail') }}{{ __('lang.list_add_oil') }}
                                                    </h5>
                                                    <button class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form>
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="table-responsive">
                                                                    <table
                                                                        class="table table-bordered table-striped text-center">
                                                                        <thead>
                                                                            <tr class="bg-light text-bold h5">
                                                                                <th colspan="10">
                                                                                    @if (!empty($this->car_data))
                                                                                        {{ $this->car_data }}
                                                                                    @endif
                                                                                </th>
                                                                            </tr>
                                                                            <tr>
                                                                                <th>{{ __('lang.no') }}</th>
                                                                                <th>{{ __('lang.date') }}</th>
                                                                                <th>{{ __('lang.gas_stations') }}</th>
                                                                                <th>{{ __('lang.qty_oil') }}</th>
                                                                                <th>{{ __('lang.price') }}/ລິດ</th>
                                                                                <th>{{ __('lang.total_paid') }}</th>
                                                                                <th>{{ __('lang.status') }}</th>
                                                                                <th>{{ __('lang.type') }}</th>
                                                                                {{-- <th>{{ __('lang.creator') }}</th> --}}
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            @php
                                                                                $i = 1;
                                                                            @endphp
                                                                            @foreach ($list_add_oil as $item)
                                                                                <tr>
                                                                                    <td>{{ $i++ }}</td>
                                                                                    <td>{{ date('d/m/Y', strtotime($item->created_at)) }}
                                                                                        <br>
                                                                                        {{ date('H:i:s', strtotime($item->created_at)) }}
                                                                                    </td>
                                                                                    <td>
                                                                                        @if (!empty($item->gas_station))
                                                                                            {{ $item->gas_station->name }}
                                                                                            <br>
                                                                                            {{ $item->gas_station->owner_name }}-{{ $item->gas_station->phone }}
                                                                                        @endif
                                                                                    </td>
                                                                                    <td>
                                                                                        {{ number_format($item->qty_oil) }}
                                                                                    </td>
                                                                                    <td>
                                                                                        {{ number_format($item->price) }}
                                                                                    </td>
                                                                                    <td class="text-bold">
                                                                                        {{ number_format($item->total_price) }}
                                                                                    </td>
                                                                                    <td>
                                                                                        @if ($item->total_price <= 0 && $item->total_paid <= 0)
                                                                                            <span class="text-success">
                                                                                                {{ __('lang.no_data') }}</span>
                                                                                        @elseif($item->total_price == $item->total_paid)
                                                                                            <span
                                                                                                class="text-success"><i
                                                                                                    class="fas fa-check-circle"></i>
                                                                                                {{ __('lang.pay_success') }}</span>
                                                                                        @elseif($item->total_price > $item->total_paid)
                                                                                            <span
                                                                                                class="text-danger"><i
                                                                                                    class="fas fa-hand-holding-usd"></i>
                                                                                                {{ __('lang.pay_not') }}</span>
                                                                                        @endif
                                                                                    </td>
                                                                                    <td>
                                                                                        @if ($item->payment_status == 1)
                                                                                            <span
                                                                                                class="text-primary">{{ __('lang.cash') }}</span>
                                                                                        @elseif($item->payment_status == 2)
                                                                                            <span
                                                                                                class="text-danger">{{ __('lang.transfer') }}</span>
                                                                                        @else
                                                                                            <span>-</span>
                                                                                        @endif
                                                                                    </td>
                                                                                    {{-- <td>
                                                                                        @if (!empty($item->user))
                                                                                        {{ $item->user->firstname }} <br>
                                                                                        @endif
                                                                                    </td> --}}
                                                                                </tr>
                                                                            @endforeach
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- =================== ໃບບິນລວມ =========================== --}}
                                    <div wire:ignore.self class="modal fabe" id="print-bill">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header bg-light">
                                                    <div class="container-fluid">
                                                        <div class="col-12">
                                                            <!-- Main content -->
                                                            <div class="invoice p-5 mb-2 mt-2" id="printContent">
                                                                <div>
                                                                    <div class="row">
                                                                        <div class="col-md-12 text-center">
                                                                            <h6>{{ __('lang.headding1') }}</h5>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12 text-center">
                                                                            <h6>{{ __('lang.headding2') }}</h5>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-5">
                                                                            <p class="login-box-msg">
                                                                                <a href="#" class="brand-link">
                                                                                    <img src="{{ asset('logo/logo.jpg') }}"
                                                                                        class="img-circle elevation-2"
                                                                                        height="80"> <br>
                                                                                    <a href="#" target="_blank"
                                                                                        class="h6 pl-4"><b>{{ __('lang.title') }}</b></a>
                                                                                </a>
                                                                            <p>
                                                                                <small>
                                                                                    @if (!empty(auth()->user()->branch))
                                                                                        @if (!empty(auth()->user()->branch->province))
                                                                                            {{ auth()->user()->branch->village->name_la }},
                                                                                            {{ auth()->user()->branch->district->name_la }},
                                                                                            {{ auth()->user()->branch->province->name_la }}
                                                                                        @endif
                                                                                        <br>
                                                                                        ໂທ:
                                                                                        @if (!empty(auth()->user()->branch))
                                                                                            {{ auth()->user()->branch->phone }}
                                                                                        @endif
                                                                                        <br>
                                                                                        {{ __('lang.bank_name') }}:
                                                                                        {{ auth()->user()->branch->bank_name }}
                                                                                        <br>
                                                                                        {{ __('lang.account_number') }}:
                                                                                        {{ auth()->user()->branch->account_number }}
                                                                                        <br>
                                                                                        {{ __('lang.account_name') }}:
                                                                                        {{ auth()->user()->branch->account_name }}
                                                                                    @endif

                                                                                </small>
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-md-7 text-right">
                                                                            <br>ວັນທີ: {{ date('d/m/Y') }}</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                        </div>
                                                                        @php
                                                                            $num = 1;
                                                                        @endphp
                                                                        {{-- <div class="col-md-6 text-right">
                                        @if (!empty($transport_data->customer))
                                            <h6><b>ລູກຄ້າ:</b> {{ $transport_data->customer->firstname }}
                                                {{ $transport_data->customer->lastname }}</h6>
                                            <h6><b>ເບີໂທ:</b> {{ $transport_data->customer->phone }}</h6>
                                        @endif
                                        <h6 class="text-success"><i class="fas fa-check-circle"></i> ຈ່າຍເເລ້ວ</h6>
                                    </div> --}}
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12 text-center">
                                                                            <h4><b>
                                                                                    ໃບຮຽກເກັບເງິນ</b>
                                                                            </h4>
                                                                        </div>
                                                                        <hr>
                                                                        {{-- @if (!empty($transport_data->start_end_streets->bor))
                                        <h6><b>ຕົ້ນທາງ:</b> {{ $transport_data->start_end_streets->bor->name }}
                                            {{ $transport_data->start_end_streets->bor->lastname }}</h6>
                                        <h6><b>ປາຍທາງ:</b> {{ $transport_data->address }}</h6>
                                    @endif --}}
                                                                    </div>
                                                                    <div wire:ignore.self class="row">
                                                                        <div class="col-md-1"></div>
                                                                        <div class="col-md-12">
                                                                            <table class="table table-bordered">
                                                                                <thead class="bg-light text-sm">
                                                                                    <tr class="text-center">
                                                                                        <th class="text-center">
                                                                                            {{ __('lang.no') }}</th>
                                                                                        <th>{{ __('lang.date') }}</th>
                                                                                        <th>ເນື້ອໃນ</th>
                                                                                        <th>ຈຳນວນຖ້ຽວ</th>
                                                                                        <th>ລວມM3</th>
                                                                                        <th>ລາຄາ/M3</th>
                                                                                        <th>ລວມເງິນ
                                                                                        </th>
                                                                                        <th>{{ __('lang.note') }}</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    @php
                                                                                        $i = 1;
                                                                                    @endphp
                                                                                    @foreach ($list_income_transport as $item)
                                                                                        <tr class="text-center">
                                                                                            <td>{{ $i++ }}
                                                                                            </td>
                                                                                            <td>{{ date('d/m/Y', strtotime($item->created_at)) }}
                                                                                            </td>
                                                                                            <td>
                                                                                                @if (!empty($item->transport->bor))
                                                                                                    {{ $item->transport->bor->bor_types_multi }}
                                                                                                @endif
                                                                                            </td>
                                                                                            <td class="text-bold">
                                                                                                {{ $item->ids }}
                                                                                            </td>
                                                                                            <td class="text-bold">
                                                                                                {{ $item->sum_kilogarms }}
                                                                                            </td>
                                                                                            <td class="text-bold">
                                                                                                {{ number_format($item->transport_prices) }}
                                                                                            </td>
                                                                                            {{-- <td></td> --}}
                                                                                            <td class="text-bold">
                                                                                                {{ number_format($item->total_transport_paids) }}
                                                                                                ₭
                                                                                            </td>
                                                                                            <td>
                                                                                            </td>
                                                                                        </tr>
                                                                                    @endforeach
                                                                                    <tr class="h6 bg-light">
                                                                                        <td colspan="3"
                                                                                            class="text-right">
                                                                                            <b>{{ __('lang.total') }}</b>
                                                                                        </td>
                                                                                        <td class="text-center">
                                                                                            <b>
                                                                                                {{ number_format($this->count_transport) }}</b>
                                                                                        </td>
                                                                                        <td class="text-center">
                                                                                            <b>
                                                                                                {{ number_format($this->sum_income_transport_kilogram) }}</b>
                                                                                        </td>
                                                                                        <td></td>
                                                                                        <td class="text-center">
                                                                                            <b>
                                                                                                {{ number_format($this->sum_income_total_transport_paid) }}</b>
                                                                                            ₭
                                                                                        </td>
                                                                                        <td>
                                                                                        </td>

                                                                                    </tr>

                                                                                </tbody>
                                                                            </table>
                                                                            <table class="table table-borderless">
                                                                                <tr class="text-center">
                                                                                    <td><b>ຜູ້ຈ່າຍເງິນ</b></td>
                                                                                    <td><b>ຜູ້ຮັບເງິນ</b></td>
                                                                                    <td><b>ຜູ້ສະຫຼຸບ</b>
                                                                                        <br>
                                                                                        @if (!empty(auth()->user()))
                                                                                            {{ auth()->user()->firstname }}
                                                                                        @endif
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        <div class="col-md-2"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="row no-print">
                                                                    <div class="col-12">
                                                                        <a href="javascript:void(0)"
                                                                            wire:click.live="ClosePrint"
                                                                            class="btn btn-warning float-left"><i
                                                                                class="fas fa-times-circle"></i>
                                                                            ປິດ</a>
                                                                        <a class="btn btn-success float-right"
                                                                            id="print" href="javascript:void(0)"
                                                                            onclick="printPage()"><i
                                                                                class="fa fa-print"></i>
                                                                            ປິ່ຣນ</a>
                                                                    </div>
                                                                </div>
                                                            </div><!-- /.invoice -->
                                                        </div><!-- /.col -->
                                                    </div><!-- /.row -->
                                                </div><!-- /.container-fluid -->
                                            </div>
                                        </div>
                                    </div>
