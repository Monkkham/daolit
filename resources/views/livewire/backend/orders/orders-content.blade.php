<div wire:poll>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h6><i class="fas fa-cart-plus"></i> ສັ່ງຊື້ <i class="fa fa-angle-double-right"></i>
                        ກະຕ່າສິນຄ້າ </h6>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('backend.dashboard') }}">ໜ້າຫຼັກ</a></li>
                        <li class="breadcrumb-item active">ກະຕ່າສິນຄ້າ</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            <div class="row">
                                {{-- <div class="col-md-4" wire:ignore>
                                    <div class="form-group">
                                        <select wire:model="product_id" id="selectProduct"
                                            class="form-control @error('product_id') is-invalid @enderror">
                                            <option value="" selected>
                                                {{ __('lang.select') }}{{ __('lang.product') }}</option>
                                            @foreach ($products as $item)
                                                <option value="{{ $item->id }}">{{ $item->name }}
                                                    {{ number_format($item->total_price) }}</option>
                                            @endforeach
                                        </select>
                                        @error('product_id')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <button wire:click="AddToCart" type="button" class="btn btn-primary"><i
                                            class="fas fa-cart-plus"></i> {{ __('lang.select') }}ໃສ່ກະຕ່າ</button>
                                </div> --}}
                                <div class="col-md-7"></div>
                                <div class="col-md-5" wire:ignore>
                                    {{-- @foreach ($res_function_available as $items)
                                        @if ($items->ResFunctions->name == 'action_34') --}}
                                    <div class="form-group">

                                        {{-- <div class="input-group"> --}}
                                        <div class="input-group row">
                                            <select wire:model="supplier_id" id="supplier_id"
                                                class="form-control @error('supplier_id') is-invalid @enderror">
                                                <option value="0" selected>
                                                    ເລືອກ-ຜູ້ສະຫນອງ</option>
                                                @foreach ($suppliers as $item)
                                                    <option value="{{ $item->id }}">
                                                        {{ $item->name_lastname }}-{{ $item->phone }}</option>
                                                @endforeach
                                            </select>
                                            <div class="input-group-append">
                                                <button type="button" wire:click="create"
                                                    class="btn btn-md btn-success">
                                                    <i class="fa fa-plus"></i> 
                                                </button>
                                            </div>
                                        </div>
                                        @error('supplier_id')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                        {{-- <div class="input-group-append">
                                                <button type="button" wire:click="addSupplier"
                                                    class="btn btn-md btn-success">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div> --}}

                                        {{-- </div> --}}
                                    </div>
                                    {{-- @endif
                                    @endforeach --}}
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped bg-light">
                                    <thead>
                                        <tr class="text-center bg-light">
                                            <th>ລຳດັບ</th>
                                            <th>ຮູບ</th>
                                            <th>ຊື່</th>
                                            <th>ລາຄາຊື້ລວມ</th>
                                            <th>ຈຳນວນ</th>
                                            <th>ເປັນເງິນ</th>
                                            <th>
                                                {{-- @foreach ($res_function_available as $items)
                                                    @if ($items->ResFunctions->name == 'action_35') --}}
                                                @if ($Count_cart)
                                                    <button type="button" class="btn btn-danger btn-sm"
                                                        wire:click="ShowClear"><i class="fas fa-trash-alt"></i>
                                                        ລຶບທັງຫມົດ
                                                    </button>
                                                @else
                                                    <button disabled type="button" class="btn btn-danger btn-sm"><i
                                                            class="fas fa-trash-alt"></i>
                                                        ລຶບທັງຫມົດ
                                                    </button>
                                                @endif
                                                {{-- @endif
                                                @endforeach --}}
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $stt = 1;
                                        @endphp

                                        @foreach ($OrdersCarts as $item)
                                            <tr>
                                                <td class="text-center">{{ $stt++ }}</td>
                                                <td class="text-center">
                                                    @if (!empty($item->product->image))
                                                        <a href="{{ asset($item->product->image) }}"target="_blank">
                                                            <img class="rounded"
                                                                src="{{ asset($item->product->image) }}" width="50px;"
                                                                height="50px;">
                                                        </a>
                                                    @else
                                                        <img src="{{ 'logo/noimage.jpg' }}" width="50px;"
                                                            height="50px;">
                                                    @endif
                                                </td>
                                                <td>{{ $item->name }}</td>
                                                <td class="text-center">
                                                    @if(!empty($item->product))
                                                    {{ number_format($item->product->total_buyprice) }} ₭
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    x{{ $item->qty }}
                                                 </td>
                                                 <td class="text-center"><b>{{ number_format($item->subtotal) }} ₭
                                                </td>
                                                <td class="text-center">
                                                    <div class="btn-group btn-group-sm btn-sm"
                                                    data-toggle="buttons">
                                                    <button type="button" class="btn btn-warning btn-sm"
                                                    wire:click="ShowQty({{ $item->id }})"> <i
                                                        class="fas fa-pen-alt"></i> ແກ້ໄຂ</button>
                                                    <button type="button" class="btn btn-danger btn-sm"
                                                    wire:click="Remove_Item({{ $item->id }})"><i
                                                        class="fas fa-times-circle"></i>
                                                </button>
                                                </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="4" class="text-right">
                                                <h5><b>ຈຳນວນ:
                                                        {{ number_format($Count_cart) }} @if ($Count_cart != 1)
                                                            ລາຍການ
                                                        @else
                                                            ລາຍການ
                                                        @endif
                                                    </b></h5>
                                            </td>
                                            <td colspan="3" class="text-center">
                                                <h5><b>ຍອດລວມ:
                                                        {{ number_format($this->sum_subtotal) }} ₭
                                                    </b></h5>
                                            </td>
                                        </tr>
                                        <tr>
                                            {{-- <td colspan="2">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group clearfix">
                                                            <div class="icheck-success d-inline">
                                                                <input type="radio" id="radioPrimary1" value="cash"
                                                                    wire:model="type" checked>
                                                                <label class="text-success" for="radioPrimary1">ເງິນສົດ
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group clearfix">
                                                            <div class="icheck-success d-inline">
                                                                <input type="radio" id="radioPrimary2"
                                                                    value="transfer" wire:model="type">
                                                                <label class="text-danger" for="radioPrimary2">ເງິນໂອນ
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @if ($this->type == 'transfer')
                                                    <div class="col-sm-4">
                                                        <div class="form-group clearfix">
                                                            <input type="file" wire:model="payment_image">
                                                        </div>
                                                    </div>
                                                    @endif
                                                </div>
                                                @error('type')
                                                    <span style="color: red" class="error">{{ $message }}</span>
                                                @enderror
                                            </td> --}}
                                            <td colspan="3">
                                                {{-- <h6><b>
                                                        <input wire:model="total_paid" placeholder="0.00"
                                                            type="text" onkeypress="validate(event)"
                                                            class="form-control money
                                                          @error('total_paid') is-invalid @enderror">
                                                        @error('total_paid')
                                                            <span style="color: red"
                                                                class="error">{{ $message }}</span>
                                                        @enderror
                                                        <span>ປ້ອນເງິນຊຳລະ</span>
                                                </h6> --}}
                                            </td>
                                            <td></td>
                                            <td></td>
                                            @if ($Count_cart)
                                                <td colspan="2">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-12 text-center">
                                                                <button wire:click="PlaceOrder"
                                                                    class="btn btn-primary btn-md btn-block"><i
                                                                        class="fas fa-credit-card"></i>
                                                                    ສັ່ງຊື້ເລີຍ</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            @else
                                                <td colspan="2">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-md-12 text-center">
                                                                <button disabled wire:click="PlaceOrder"
                                                                    class="btn btn-primary btn-md btn-block"><i
                                                                        class="fas fa-credit-card"></i>
                                                                    ສັ່ງຊື້ເລີຍ</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            @endif
                                        </tr>
                                    </tfoot>

                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('livewire.backend.orders.orders-modal-form')
    @include('livewire.backend.data-store.modal-script')
</div>
