<div wire:poll>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

                <div class="col-sm-6">
                    <h6><i class="fas fa-cart-plus"></i> ຂາຍອອກ <i class="fa fa-angle-double-right"></i>
                        ລາຍການທີ່ຂາຍອອກ</h6>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('backend.dashboard') }}">ໜ້າຫຼັກ</a>
                        </li>
                        <li class="breadcrumb-item active">ລາຍການທີ່ຂາຍອອກ</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="date" wire:model="start_date" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="date" wire:model="end_date" class="form-control">
                                    </div>
                                </div>
                                {{-- <div class="col-md-2">
                                    <input wire:model.live="search" type="text" class="form-control"
                                        placeholder="ຄົ້ນຫາ...">
                                </div> --}}
                                {{-- <div class="col-md-2">
                                    <div class="form-group">
                                        <select wire:model="status" class="form-control">
                                            <option value="" selected>ເລືອກ-ສະຖານະ</option>
                                            <option value="1">ລໍຖ້າຮັບ</option>
                                            <option value="2">ນຳເຂົ້າສຳເລັດ</option>
                                        </select>
                                    </div>
                                </div> --}}
                                {{-- <div class="col-md-2">
                                    <div class="form-group">
                                        <select wire:model="status" class="form-control">
                                            <option value="" selected>ເລືອກ-ຊຳລະ</option>
                                            <option value="1">ຄ້າງຊຳລະ</option>
                                            <option value="2">ຊຳລະເເລ້ວ</option>
                                        </select>
                                    </div>
                                </div> --}}
                                <div class="col-md-5"></div>
                                <div class="col-md-1">
                                    {{-- <a href="{{route('backend.order_add')}}" type="button" class="btn btn-warning" style="width: auto;"><i class="fa fa-cart-plus"></i> {{__('lang.purchase_orders')}}</a> --}}
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped text-sm">
                                    <thead>
                                        <tr class="text-center bg-light">
                                            <th>ຈັດການ</th>
                                            <th>ລຳດັບ</th>
                                            <th>ວັນທີ</th>
                                            <th>ລູກຄ້າ</th>
                                            <th>ຍອດລວມ</th>
                                            <th>ຍອດຊຳລະ</th>
                                            <th>ຍອດຫນີ້</th>
                                            <th>ສະຖານະ</th>
                                            <th>ຜູ້ສ້າງ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $num = 1;
                                        @endphp
                                        @foreach ($data as $item)
                                            <tr class="text-center">
                                                <td style="text-align: center">
                                                    <div wire:ignore class="btn-group btn-left">
                                                        @if ($item->check_payment != 1)
                                                            <button wire:click="ShowPayment({{ $item->id }})"
                                                                type="button" class="btn btn-info btn-sm"><i
                                                                    class="fas fa-hand-holding-usd"></i></button>
                                                            {{-- <button wire:click="ShowUpdate({{ $item->id }})"
                                                                type="button" class="btn btn-warning btn-sm"><i
                                                                    class="fas fa-edit"></i></button>
                                                            <button wire:click="ShowUpdate({{ $item->id }})"
                                                                type="button" class="btn btn-danger btn-sm"><i
                                                                    class="fas fa-trash"></i></button> --}}
                                                        @else
                                                            <button wire:click="ShowPayment({{ $item->id }})"
                                                                type="button" class="btn btn-success btn-sm"><i
                                                                    class="fas fa-check-circle"></i> </button>
                                                        @endif
                                                        <button wire:click="ShowUpdate('{{ $item->id }}')" type="button"
                                                            class="btn btn-warning btn-sm"><i
                                                                class="fas fa-pen"></i></button>
                                                                <button wire:click="showDestory('{{ $item->id }}')" type="button"
                                                                    class="btn btn-danger btn-sm"><i
                                                                        class="fas fa-trash"></i></button>

                                                        {{-- <button type="button"
                                                            class="btn btn-info btn-sm dropdown-toggle dropdown-icon"
                                                            data-toggle="dropdown">ຈັດການ
                                                        </button> --}}
                                                        <div class="dropdown-menu" role="menu">
                                                            @if ($item->status != '2')
                                                                {{-- @foreach ($res_function_available as $items)
                                                                    @if ($items->ResFunctions->name == 'action_37') --}}
                                                                {{-- <a class="dropdown-item" href="javascript:void(0)"
                                                                    wire:click="ShowConfirm({{ $item->id }})"><i
                                                                        class="fas fa-file-import">
                                                                    </i> ນຳເຂົ້າສາງ</a> --}}
                                                            @endif
                                                            {{-- @endforeach --}}
                                                            {{-- <li class="dropdown-divider"></li> --}}
                                                            {{-- @foreach ($res_function_available as $items)
                                                                    @if ($items->ResFunctions->name == 'action_40') --}}
                                                            {{-- <a class="dropdown-item"
                                                                wire:click='ShowUpdate({{ $item->id }})'
                                                                href="javascript:void(0)"><i class="fas fa-edit"></i>
                                                                ແກ້ໄຂ</a> --}}
                                                            {{-- @endif
                                                                @endforeach

                                                            @endif --}}
                                                            <a class="dropdown-item" href="javascript:void(0)"
                                                                wire:click="UpdateImport({{ $item->id }})"><i
                                                                    class="fas fa-pencil-alt">
                                                                    {{ __('lang.edit') }}</i></a>
                                                            @if ($item->total_money - $item->total_paid != 0)
                                                                {{-- @foreach ($res_function_available as $items)
                                                                    @if ($items->ResFunctions->name == 'action_38') --}}
                                                                @if ($item->total == $item->sales_logs_sum_total_paid)
                                                                    <a class="dropdown-item" href="javascript:void(0)"
                                                                        wire:click="ShowPayment({{ $item->id }})"><i
                                                                            class="fas fa-history"></i>
                                                                        ປະຫັວດຊຳລະ</a>
                                                                @else
                                                                    <a class="dropdown-item" href="javascript:void(0)"
                                                                        wire:click="ShowPayment({{ $item->id }})"><i
                                                                            class="fas fa-hand-holding-usd"></i>
                                                                        ຊຳລະຫນີ້</a>
                                                                @endif
                                                            @endif
                                                            {{-- @endforeach
                                                            @else --}}
                                                            {{-- @foreach ($res_function_available as $items)
                                                                    @if ($items->ResFunctions->name == 'action_38') --}}
                                                            {{-- @endif
                                                                @endforeach --}}
                                                            {{-- @endif --}}
                                                            {{-- @foreach ($res_function_available as $items)
                                                                @if ($items->ResFunctions->name == 'action_40') --}}
                                                            <a class="dropdown-item" href="javascript:void(0)"
                                                                wire:click="ShowBill({{ $item->id }})"><i
                                                                    class="fas fa-print">
                                                                </i> ພິມບິນ</a>

                                                            </a>
                                                            {{-- @endif
                                                            @endforeach --}}
                                                            {{-- @foreach ($res_function_available as $items)
                                                                @if ($items->ResFunctions->name == 'action_40') --}}
                                                            {{-- <a class="dropdown-item" href="javascript:void(0)"
                                                                wire:click="ShowDetail({{ $item->id }})"><i
                                                                    class="fas fa-file"></i>
                                                                ລາຍລະອຽດ</a> --}}
                                                            </a>
                                                            {{-- @endif
                                                            @endforeach --}}
                                                            {{-- <li class="dropdown-divider"></li> --}}
                                                            {{-- <a class="dropdown-item" href="javascript:void(0)"
                                                                wire:click=""><i class="fas fa-times-circle">
                                                                    ຍົກເລີກ</i></a> --}}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>{{ $num++ }}</td>
                                                <td>{{ date('d/m/Y', strtotime($item->created_at)) }}</td>
                                                <td>
                                                    @if (!empty($item->customer))
                                                        {{ $item->customer->name_lastname }} <br>
                                                        {{ $item->customer->phone }}
                                                    @endif
                                                </td>
                                                <td class="text-primary">
                                                    {{ number_format($item->sales_detail_sum_subtotal) }}
                                                    ₭ <br>(ຄ່າສົ່ງ {{ number_format($item->shipping_price) }})</td>
                                                <td class="text-success">
                                                    {{ number_format($item->sales_logs_sum_total_paid) }}
                                                    ₭</td>
                                                <td class="text-danger">
                                                    {{ number_format(($item->total + $item->shipping_price) - $item->sales_logs_sum_total_paid) }}
                                                    ₭
                                                </td>
                                                <td>
                                                    @if (($item->total + $item->shipping_price) == $item->sales_logs_sum_total_paid)
                                                        <span class="text-success fas fa-check-circle">
                                                            ຈ່າຍຄົບເເລ້ວ</span>
                                                    @elseif(($item->total + $item->shipping_price) > $item->sales_logs_sum_total_paid)
                                                        <span class="text-danger fas fa-hand-holding-usd">
                                                            ຄ້າງຈ່າຍ</span>
                                                    @endif
                                                </td>
                                                {{-- <td>
                                                    @if ($item->status == '1')
                                                        <p class="text-warning"><span
                                                                class="spinner-grow spinner-grow-sm" role="status"
                                                                aria-hidden="true"></span> ລໍຖ້າຮັບ</p>
                                                    @elseif($item->status == '2')
                                                        <p class="text-success"><i class="fas fa-check-circle"></i>
                                                            ນຳເຂົ້າສຳເລັດ</p>
                                                    @elseif($item->status == '3')
                                                        <p class="text-danger"><i class="fas fa-times-circle"></i>
                                                            ຍົກເລີກ</p>
                                                    @endif
                                                </td> --}}
                                                <td>
                                                    @if (!empty($item->employee))
                                                        {{ $item->employee->name_lastname }}
                                                    @endif
                                                </td>

                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                                {{-- <div class="float-right">
                                    {{ $data->links() }}
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- \\\\\\\\\\\\\\\\\\\\\\\ history Pay money \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ --}}
    <div class="modal fade" id="modal-paymoney" wire:ignore.self>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    @if ($this->total > 0)
                        <h4 class="modal-title"><i class="fas fa-hand-holding-usd"></i> ຊຳລະຫນີ້
                        </h4>
                    @else
                        <h4 class="modal-title"><i class="fas fa-history"></i> ປະຫວັດການຊຳລະ
                        </h4>
                    @endif
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row text-center">
                        <input type="hidden" wire:model="ID">
                        <div class="col-md-12">
                            <h4>ຍອດຫນີ້ຕ້ອງຊຳລະ</h4>
                            <h3 class="text-danger">{{ number_format($this->total) }} ₭
                            </h3>
                        </div>
                    </div>
                    @if ($this->total > 0)
                        <td>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group clearfix">
                                        <div class="icheck-success d-inline">
                                            <input type="radio" id="radioPrimary1" value="1"
                                                wire:model="type" checked>
                                            <label class="text-success" for="radioPrimary1">ເງິນສົດ
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group clearfix">
                                        <div class="icheck-danger d-inline">
                                            <input type="radio" id="radioPrimary2" value="2"
                                                wire:model="type">
                                            <label class="text-danger" for="radioPrimary2">ເງິນໂອນ
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                {{-- @if ($this->type == '2')
                                            <div class="col-sm-4">
                                                <div class="form-group clearfix">
                                                    <input type="file" wire:model="payment_image">
                                                </div>
                                            </div>
                                        @endif --}}
                            </div>
                            @error('type')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </td>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>ຍອດຊຳລະ</label>
                                    <input wire:model="total_paid" placeholder="0.00" type="text"
                                        class="form-control money @error('total_paid') is-invalid @enderror">
                                    @error('total_paid')
                                        <span style="color: #ff0000" class="error">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-center">
                            <td colspan="2">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <button wire:click="ConfirmPayment"
                                                class="btn btn-success btn-md btn-block"><i
                                                    class="fas fa-credit-card"></i>
                                                ຍືນຍັນຊຳລະ</button>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            {{-- <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                                            class="fas fa-times-circle"></i> {{ __('lang.cancel') }}</button> --}}
                        </div>
                    @endif
                    <table class="table table-hover">
                        <thead class="bg-danger text-center">
                            <tr>
                                <th>ລຳດັບ</th>
                                <th>ວັນທີ</th>
                                <th>ເປັນເງິນ</th>
                                <th>ການຊຳລະ</th>
                                {{-- <th>ຮູບ</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $num = 1;
                            @endphp
                            @foreach ($orders_logs as $item)
                                <tr class="text-center">
                                    <td>{{ $num++ }}</td>
                                    <td>{{ date('d/m/Y', strtotime($item->created_at)) }}</td>
                                    <td>{{ number_format($item->total_paid) }} ₭</td>
                                    <td>
                                        @if ($item->type == '1')
                                            <p class="text-success">ເງິນສົດ</p>
                                        @elseif($item->type == '2')
                                            <p class="text-danger">ເງິນໂອນ</p>
                                        @endif
                                    </td>
                                    {{-- <td>
                                                @if (!empty($item->payment_image))
                                                    <a href="{{ asset($item->payment_image) }}" target="_blank">
                                                        <img class="rounded" src="{{ asset($item->payment_image) }}"
                                                            width="50px;" height="50px;">
                                                    </a>
                                                @else
                                                    <img src="{{ 'logo/noimage.jpg' }}" width="50px;" height="50px;">
                                                @endif
                                            </td> --}}
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
        {{-- \\\\\\\\\\\\\\\\\\\\\\\ Edit order item \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ --}}
        <div class="modal fade" id="modal-update-item" wire:ignore.self>
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"><i class="fas fa-cart-plus"></i> ແກ້ໄຂໃບບິນ {{ $this->code }}
                        </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row text-center">
                            <input type="hidden" wire:model="ID">
                            <div class="col-md-12">
                                <h4>ລາຍການສັ່ງຊື້</h4>
                            </div>
                        </div>
                        <table class="table table-hover text-center responsive">
                            <thead class="bg-light text-center">
                                <tr>
                                    <th>ລຳດັບ</th>
                                    <th>ສິນຄ້າ</th>
                                    <th>ຈຳນວນ</th>
                                    <th>ລາຄາ</th>
                                    <th>ເປັນເງິນ</th>
                                    <th>ຈັດການ</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $num = 1;
                                @endphp
                                @foreach ($salesDetail as $item)
                                    <tr class="text-center">
                                        <td>{{ $num++ }}</td>
                                        <td>
                                            @if (!empty($item->product))
                                                {{ $item->product->name }}
                                            @endif
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input wire:model="stock.{{ $item->id }}" placeholder="0.00" style="width: 80px"
                                                            value="{{ $item->stock }}" min="1" type="number"
                                                            class="form-control text-center money @error('stock.' . $item->id) is-invalid @enderror"
                                                            wire:change="UpdateStock({{ $item->id }})">
                                                        @error('stock.' . $item->id)
                                                            <span style="color: #ff0000"
                                                                class="error">{{ $message }}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6 text-left">
                                                    <div class="form-group">
                                                        {{ $item->stock }}
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            {{ number_format($item->sell_price) }} ₭
                                        </td>
                                        <td>
                                            {{ number_format($item->subtotal) }} ₭
                                        </td>
                                        <td>
                                            <button wire:click="Remove_Item({{ $item->id }})"
                                                class="btn btn-danger btn-sm"><i class="fas fa-times-circle"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
            <!-- /.modal-delete-->
    <div wire:ignore class="modal fabe" id="modal-delete">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title"><i class="fa fa-trash"> </i> ລຶບອອກ</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h3 class="text-center">ທ່ານຕ້ອງການລຶບຂໍ້ມູນນີ້ອອກບໍ່?</h3>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">ຍົກເລີກ</button>
                    <button wire:click="Destroy({{ $ID }})" type="button" class="btn btn-success"><i
                            class="fa fa-trash"></i> ລຶບອອກ</button>
                </div>
            </div>
        </div>
    </div>
    {{-- @include('livewire.backend.orders.import-modal') --}}
    @include('livewire.backend.data-store.modal-script')
</div>
