<?php

namespace App\Http\Livewire\Backend;

use App\Models\IncomeExpend;
use App\Models\Orders;
use App\Models\Sales;
use Livewire\Component;

class DashboardContent extends Component
{
    public function render()
    {
        $orders = Orders::withSum('orders_detail', 'subtotal')->get();
        $sum_orders_total = $orders->sum(function ($order) {
            return $order->orders_detail_sum_subtotal ?? 0;
        });

        $sales = Sales::withSum('sales_detail', 'subtotal')->get();
        $sum_shipping_price = $sales->sum('shipping_price');
        $sum_sales_total = $sales->sum(function ($sale) {
            return $sale->sales_detail_sum_subtotal ?? 0;
        });

        $sales_not_paid = Sales::withSum('sales_logs', 'total_paid')->get();
        $sum_sales_not_total = $sales_not_paid->sum(function ($sale) {
            return $sale->sales_logs_sum_total_paid ?? 0;
        });

        $income_total = IncomeExpend::select('total_price')->where('type', 1)->sum('total_price');
        $expend_total = IncomeExpend::select('total_price')->where('type', 2)->sum('total_price');
        return view('livewire.backend.dashboard-content', compact('sum_orders_total', 'sum_sales_total', 'sum_shipping_price','income_total', 'expend_total','sum_sales_not_total'))->layout('layouts.backend.style');
    }
}
