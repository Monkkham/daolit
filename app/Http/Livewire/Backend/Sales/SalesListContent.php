<?php

namespace App\Http\Livewire\Backend\Sales;

use App\Models\Sales;
use App\Models\SalesDetail;
use App\Models\SalesLogs;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class SalesListContent extends Component
{
    use WithFileUploads;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search, $start_date, $end_date, $status, $ID, $caculate;
    public function mount()
    {
        $this->start_date = date('Y-m-d');
        $this->end_date = date('Y-m-d');
    }
    public function render()
    {
        $end = date('Y-m-d H:i:s', strtotime($this->end_date . '23:23:59'));
        $data = Sales::withSum('sales_detail', 'subtotal')->withSum('sales_logs', 'total_paid')
            ->where(function ($query) {
                $query->where('code', 'like', '%' . $this->search . '%');
            });
        if ($this->start_date && $this->end_date) {
            $data = $data->whereBetween('created_at', [$this->start_date, $end]);
        }
        if (!empty($data)) {
            $data = $data->get();
        } else {
            $data = [];
        }
        return view('livewire.backend.sales.sales-list-content', compact('data'))->layout('layouts.backend.style');
    }
    public function resetField()
    {
        $this->total_paid = '';
        // $this->payment_image = '';
        $this->type = '';
    }
    public $orders_logs = [], $sum_subtotal_paid, $total, $payment_logs = [], $total_paid, $type = 2;
    public function ShowPayment($id)
    {
        $this->resetField();
        $this->dispatchBrowserEvent('show-modal-paymoney');
        $sales = Sales::find($id);
        $this->ID = $sales->id;
        $this->sum_subtotal_paid = SalesLogs::select('total_paid')->where('sales_id', $this->ID)->sum('total_paid');
        $this->orders_logs = SalesLogs::where('sales_id', $this->ID)->get();
        $this->total = ($sales->total + $sales->shipping_price) - $this->sum_subtotal_paid;
    }
    public function ConfirmPayment()
    {
        $this->validate([
            'total_paid' => 'required',
            'type' => 'required',
        ], [
            'total_paid.required' => 'ປ້ອນຂໍ້ມູນກ່ອນ',
            'type.required' => 'ເລືອກຂໍ້ມູນກ່ອນ',
        ]);
        $sales = Sales::find($this->ID);
        $this->sum_subtotal_paid = SalesLogs::select('total_paid')->where('sales_id', $sales->id)->sum('total_paid');
        if ((($sales->total + $sales->shipping_price) - $this->sum_subtotal_paid) < str_replace(',', '', $this->total_paid)) {
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ທ່ານປ້ອນເງິນເກີນຍອດຫນີ້!',
                'icon' => 'warning',
                'iconColor' => 'red',
            ]);
        } else {
            if ($sales->total == $this->sum_subtotal_paid) {
                $sales->check_payment = "2";
            } elseif ($sales->total > $this->sum_subtotal_paid) {
                $sales->check_payment = "1";
            } elseif ($sales->total == $this->sum_subtotal_paid) {
                $sales->check_payment = "1";
            }
            $sales->update();
            $sales_logs = new SalesLogs();
            $sales_logs->sales_id = $sales->id;
            $sales_logs->total_paid = str_replace(',', '', $this->total_paid);
            $sales_logs->type = $this->type;
            $sales_logs->dated = Carbon::now();
            // if (!empty($this->payment_image)) {
            //     $this->validate([
            //         'payment_image' => 'required|mimes:jpg,png,jpeg',
            //     ]);
            //     $imageName = Carbon::now()->timestamp . '.' . $this->payment_image->extension();
            //     $this->payment_image->storeAs('upload/payment', $imageName);
            //     $sales_logs->payment_image = 'upload/payment' . '/' . $imageName;
            // } else {
            //     $sales_logs->payment_image = '';
            // }
            $sales_logs->save();
            $this->dispatchBrowserEvent('hide-modal-paymoney');
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ຊຳລະຫນີ້ສຳເລັດ!',
                'icon' => 'success',
                'iconColor' => 'green',
            ]);
            return redirect(route('backend.SalesList'));
        }
    }
    public $salesDetail = [], $stock = [], $code;
    public function ShowUpdate($ids)
    {
        $this->dispatchBrowserEvent('show-modal-update-item');
        $sales = Sales::find($ids);
        $this->ID = $sales->id;
        $this->code = $sales->code;
        $this->salesDetail = SalesDetail::where('sales_id', $this->ID)->get();
        $this->stock = $this->salesDetail->pluck('stock');
    }

    public function UpdateStock($id)
    {
        $salesDetail = SalesDetail::find($id);
        $salesDetail->stock = $this->stock[$id];
        $salesDetail->subtotal = $salesDetail->sell_price * $this->stock[$id];
        $salesDetail->save();
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ແກ້ໄຂຈຳນວນສຳເລັດ!',
            'icon' => 'success',
        ]);
    }

    public function Remove_Item($id)
    {
        $SalesDetail = SalesDetail::find($id);
        $SalesDetail->delete();
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ລຶບຂໍ້ມູນສຳເລັດ!',
            'icon' => 'success',
        ]);
    }

    public function showDestory($ids)
    {
        $this->ID = $ids;
        $data = Sales::find($ids);
        // $this->name = $data->name;
        $this->dispatchBrowserEvent('show-modal-delete');
    }

    public function Destroy()
    {
        $sales = Sales::find($this->ID);

        if (!$sales) {
            return;
        }

        $salesDetails = SalesDetail::where('sales_id', $this->ID)->get();
        foreach ($salesDetails as $salesDetail) {
            $salesDetail->delete();
        }
        $salesLogs = SalesLogs::where('sales_id', $this->ID)->get();
        foreach ($salesLogs as $salesLog) {
            $salesLog->delete();
        }
        $sales->delete();
        $this->resetField();

        $this->dispatchBrowserEvent('hide-modal-delete');
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ສຳເລັດເເລ້ວ!',
            'icon' => 'success',
        ]);
        return redirect(route('backend.SalesList'));
    }

}
