<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    use HasFactory;
    protected $table = 'sales';
    protected $fillable = [
        'employee_id',
        'customer_id',
        'code',
        'total',
        'shipping_price',
        'status',
        'type',
        'onepay',
        'note',
        'check_payment',
        'created_at',
        'updated_at',
    ];
    public function employee()
    {
        return $this->belongsTo('App\Models\User', 'employee_id', 'id');
    }
    public function customer()
    {
        return $this->belongsTo('App\Models\User', 'customer_id', 'id');
    }
    public function sales_logs()
    {
        // return $this->hasMany(SalesLogs::class);
        return $this->hasMany(SalesLogs::class, 'sales_id', 'id');

    }
    public function sales_detail()
    {
        return $this->hasMany(SalesDetail::class);
    }
}
