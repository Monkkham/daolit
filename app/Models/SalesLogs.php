<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SalesLogs extends Model
{
    use HasFactory;
    protected $table = 'sales_logs';
    protected $fillable = [
        'sales_id',
        'total_paid',
        'type',
        'dated',
        'created_at',
        'updated_at',
    ];
    public function sales()
    {
        return $this->belongsTo('App\Models\Sales', 'sales_id', 'id');
    }
}
